<?php

namespace App\Http\Controllers;

use App\Models\Tipo;
use App\Models\Produto;
use App\Models\UnidadeMedida;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ProdutosController extends Controller
{
    /**
     * Display a listing of the produtos.
     *
     * @return Illuminate\View\View
     */

    public function index()
    {
        $produtos = produto::with('tipo','unidademedida')->paginate(25);
        return view('produtos.index', compact('produtos'));
    }

    /**
     * Show the form for creating a new produto.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $tbtipos = Tipo::pluck('descricao','cdtipo')->all();
        $tbunidademedidas = UnidadeMedida::pluck('unidadeMedida','cdunidademedida')->all();

        
        return view('produtos.create', compact('tbtipos','tbunidademedidas'));
    }

    /**
     * Store a new produto in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            produto::create($data);

            return redirect()->route('produtos.produto.index')
                             ->with('success_message', 'Produto was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => $exception->getMessage()]);
        }
    }

    /**
     * Display the specified produto.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $produto = produto::with('tipo','unidademedida')->findOrFail($id);

        return view('produtos.show', compact('produto'));
    }

    /**
     * Show the form for editing the specified produto.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $produto = produto::findOrFail($id);
        $tbtipos = Tipo::pluck('descricao','cdtipo')->all();
        $tbunidademedidas = UnidadeMedida::pluck('unidadeMedida','cdunidademedida')->all();


        return view('produtos.edit', compact('produto','tbtipos','tbunidademedidas'));
    }

    /**
     * Update the specified produto in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $produto = produto::findOrFail($id);
            $produto->update($data);
            return redirect()->route('produtos.produto.index')
                             ->with('success_message', 'Produto was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified produto from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $produto = produto::findOrFail($id);
            $produto->delete();

            return redirect()->route('produtos.produto.index')
                             ->with('success_message', 'Produto was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'nmProduto' => 'required|string|min:1|max:45',
            'qtdEstoque' => 'required|numeric|',
            'cdTipo' => 'required',
            'cdUnidadeMedida' => 'required',
            'qtdMinima' => 'required|numeric|',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

    public function sessao(Request $request)
{
    session(['origem'=>'produtos']);
    session(['nome' => $request->input('nmProduto')]);
    session(['qtd' => $request->input('qtdMinima')]);
    session(['cdTipo' => $request->input('cdTipo')]); 
    session(['cdUnidadeMedida' => $request->input('cdUnidadeMedida')]);
    if ($request->input('novoProduto')=="true"){
        session(['novoProduto' => true]);
    }
    session(['data'=>$request->input('dataSession')]);
    $tipoEntrada = $request->input('tipoEntrada');
    if($tipoEntrada=="0" || $tipoEntrada=="1"){
        session(['tipoEntrada' => $tipoEntrada]);
        $produtos = $request->input('produtos');
        $precos = $request->input('precos');
        $quantidades = $request->input('quantidades');
        if (!empty($produtos)){
            session(['produtos' => $produtos]); 
            session(['precos' => $precos]); 
            session(['quantidades' => $quantidades]);
        }   
    }

    if ($request->input('destino')=="tipo")
        return redirect()->route('tipos.tipo.create');
    else if ($request->input('destino')=="unidadeMedida") 
        return redirect()->route('unidade_medidas.unidade_medida.create');
    else if ($request->input('destino')=="transacao") {
        session(['novoProduto'=>'true']);
        return redirect()->route('produtos.produto.create');
    }
    else
        return redirect()->route('produtos.produto.create');
}

}

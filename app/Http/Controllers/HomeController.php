<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\TransacaoEntradas;
use App\Http\Controllers\Controller;
use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the produtos.
     *
     * @return Illuminate\View\View
     */
    public function inicio()
    {
        $produtos = Produto::with('tipo','unidademedida')->orderBy('nmProduto', 'asc')->get();
        $transacoes = TransacaoEntradas::orderBy('cdTransacao', 'desc')->paginate(10);
        return view('home', compact('produtos','transacoes'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}

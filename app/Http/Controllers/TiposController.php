<?php

namespace App\Http\Controllers;

use App\Models\Tipo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class TiposController extends Controller
{

    /**
     * Display a listing of the tipos.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $tipos = Tipo::paginate(25);

        return view('tipos.index', compact('tipos'));
    }

    /**
     * Show the form for creating a new tipo.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('tipos.create');
    }

    /**
     * Store a new tipo in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Tipo::create($data);
            if ($request->input('novoProduto')=="true")
                session(['novoProduto' => true]);
            if($request->input('origem')=='produtos')
                return redirect()->route('produtos.produto.create')->with('success_message', 'Tipo was successfully added!');
            return redirect()->route('tipos.tipo.index')
                             ->with('success_message', 'Tipo was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified tipo.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tipo = Tipo::findOrFail($id);

        return view('tipos.show', compact('tipo'));
    }

    /**
     * Show the form for editing the specified tipo.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tipo = Tipo::findOrFail($id);
        

        return view('tipos.edit', compact('tipo'));
    }

    /**
     * Update the specified tipo in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tipo = Tipo::findOrFail($id);
            $tipo->update($data);
            

            return redirect()->route('tipos.tipo.index')
                             ->with('success_message', 'Tipo was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified tipo from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tipo = Tipo::findOrFail($id);
            $tipo->delete();

            return redirect()->route('tipos.tipo.index')
                             ->with('success_message', 'Tipo was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'descricao' => 'required|string|min:1|max:45',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UnidadeMedida;
use App\Http\Controllers\Controller;
use Exception;

class UnidadeMedidasController extends Controller
{

    /**
     * Display a listing of the unidade medidas.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $unidadeMedidas = UnidadeMedida::paginate(25);

        return view('unidade_medidas.index', compact('unidadeMedidas'));
    }

    /**
     * Show the form for creating a new unidade medida.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('unidade_medidas.create');
    }

    /**
     * Store a new unidade medida in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            UnidadeMedida::create($data);
            if ($request->input('novoProduto')=="true")
                session(['novoProduto' => true]);
            if($request->input('origem')=='produtos')
                return redirect()->route('produtos.produto.create')->with('success_message', 'Unidade Medida was successfully added!');
            return redirect()->route('unidade_medidas.unidade_medida.index')
                             ->with('success_message', 'Unidade Medida was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified unidade medida.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $unidadeMedida = UnidadeMedida::findOrFail($id);

        return view('unidade_medidas.show', compact('unidadeMedida'));
    }

    /**
     * Show the form for editing the specified unidade medida.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $unidadeMedida = UnidadeMedida::findOrFail($id);
        

        return view('unidade_medidas.edit', compact('unidadeMedida'));
    }

    /**
     * Update the specified unidade medida in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $unidadeMedida = UnidadeMedida::findOrFail($id);
            $unidadeMedida->update($data);

            return redirect()->route('unidade_medidas.unidade_medida.index')
                             ->with('success_message', 'Unidade Medida was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified unidade medida from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $unidadeMedida = UnidadeMedida::findOrFail($id);
            $unidadeMedida->delete();

            return redirect()->route('unidade_medidas.unidade_medida.index')
                             ->with('success_message', 'Unidade Medida was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'unidadeMedida' => 'required|string|min:1|max:45',
            'abreviatura' => 'required|string|min:1|max:10',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}

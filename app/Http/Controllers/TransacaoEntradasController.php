<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransacaoEntradas;
use App\Http\Controllers\Controller;
use Exception;
use App\Models\Produto;
use App\Models\Tipo;
use App\Models\UnidadeMedida;

class TransacaoEntradasController extends Controller
{

    /**
     * Display a listing of the transacao entradas.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $transacao_entradas = TransacaoEntradas::paginate(5);

        return view('transacao_entradas.index', compact('transacao_entradas'));
    }

    /**
     * Show the form for creating a new transacao entradas.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        $produtos = Produto::all();
        $tbtipos = Tipo::pluck('descricao','cdtipo')->all();
        $tbunidademedidas = UnidadeMedida::pluck('unidadeMedida','cdunidademedida')->all();
        return view('transacao_entradas.create', compact('produtos', 'tbtipos','tbunidademedidas'));
    }

    /**
     * Store a new transacao entradas in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $valor = $request->input('precos');
        $data = $this->getData($request);
        try {
            
            $data = $this->getData($request);
            $produtos = $request->input('produtos');
            $precos = $request->input('precos');
            $quantidades = $request->input('quantidades');
            $transacao = TransacaoEntradas::create($data);
            if(!empty($produtos)){
                foreach($produtos as $index => $p){
                    $transacao->produto()->attach($p, ['qtd'=>$quantidades[$index], 'valor'=>$precos[$index]]);
                    $produto = produto::findOrFail($p);
                    $produto->qtdEstoque = $produto->qtdEstoque + $quantidades[$index];
                    $produto->save();
                }
            }
            return redirect()->route('transacao_entradas.transacao_entradas.create')
                             ->with('success_message', 'Transacao Entradas was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => " ALÔ ".$valor[0]."; ".$exception->getMessage()]);
        }
    }

    /**
     * Display the specified transacao entradas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $transacao_entradas = TransacaoEntradas::findOrFail($id);

        return view('transacao_entradas.show', compact('transacao_entradas'));
    }

    /**
     * Show the form for editing the specified transacao entradas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $transacao_entradas = TransacaoEntradas::findOrFail($id);
        

        return view('transacao_entradas.edit', compact('transacao_entradas'));
    }

    /**
     * Update the specified transacao entradas in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $transacao_entradas = TransacaoEntradas::findOrFail($id);
            $transacao_entradas->update($data);

            return redirect()->route('transacao_entradas.transacao_entradas.index')
                             ->with('success_message', 'Transacao Entradas was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified transacao entradas from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $transacao_entradas = TransacaoEntradas::findOrFail($id);
            $transacao_entradas->delete();

            return redirect()->route('transacao_entradas.transacao_entradas.index')
                             ->with('success_message', 'Transacao Entradas was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'data' => 'required',
            'valorTotal' => 'required',
            'eDoacao' => 'required',
            'tipo' => 'required'
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}

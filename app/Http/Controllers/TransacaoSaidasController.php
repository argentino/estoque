<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransacaoSaidas;
use App\Http\Controllers\Controller;
use Exception;
use App\Models\Produto;

class TransacaoSaidasController extends Controller
{

    /**
     * Display a listing of the transacao saidas.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $transacao_saidas = TransacaoSaidas::paginate(5);

        return view('transacao_saidas.index', compact('transacao_saidas'));
    }

    /**
     * Show the form for creating a new transacao saidas.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        $produtos = Produto::all();
        return view('transacao_saidas.create', compact('produtos'));
    }

    /**
     * Store a new transacao saidas in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $transacao = TransacaoSaidas::create($data);
            $produtos = $request->input('produtos');
            $quantidades = $request->input('quantidades');
            if(!empty($produtos)){
                foreach($produtos as $index => $p){
                    $transacao->produto()->attach($p, ['qtd'=>$quantidades[$index], 'valor'=>0]);
                    $produto = produto::findOrFail($p);
                    $produto->qtdEstoque = $produto->qtdEstoque - $quantidades[$index];
                    $produto->save();
                }
            }
            return redirect()->route('transacao_saidas.transacao_saidas.create')
                             ->with('success_message', 'Transacao Saidas was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => $exception->getMessage()]);
        }
    }

    /**
     * Display the specified transacao saidas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $transacao_saidas = TransacaoSaidas::findOrFail($id);

        return view('transacao_saidas.show', compact('transacao_saidas'));
    }

    /**
     * Show the form for editing the specified transacao saidas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $transacao_saidas = TransacaoSaidas::findOrFail($id);
        

        return view('transacao_saidas.edit', compact('transacao_saidas'));
    }

    /**
     * Update the specified transacao saidas in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $transacao_saidas = TransacaoSaidas::findOrFail($id);
            $transacao_saidas->update($data);

            return redirect()->route('transacao_saidas.transacao_saidas.index')
                             ->with('success_message', 'Transacao Saidas was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified transacao saidas from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $transacao_saidas = TransacaoSaidas::findOrFail($id);
            $transacao_saidas->delete();

            return redirect()->route('transacao_saidas.transacao_saidas.index')
                             ->with('success_message', 'Transacao Saidas was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'data' => 'required',
            'valorTotal' => 'required',
            'eDoacao' => 'required',
            'tipo' => 'required'
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbproduto';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cdProduto';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nmProduto',
                  'qtdEstoque',
                  'cdTipo',
                  'cdUnidadeMedida',
                  'qtdMinima'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tbtipo for this model.
     */
    public function tipo()
    {
        return $this->belongsTo('App\Models\Tipo','cdTipo','cdTipo');
    }

    /**
     * Get the tbunidademedida for this model.
     */
    public function unidademedida()
    {
        return $this->belongsTo('App\Models\UnidadeMedida','cdUnidadeMedida','cdUnidadeMedida');
    }

}

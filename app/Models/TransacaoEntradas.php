<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransacaoEntradas extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbtransacao';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cdTransacao';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'data',
                  'valorTotal',
                  'eDoacao',
                  'tipo'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tbtransacaoTbproduto for this model.
     */
    public function produto()
    {
        return $this->belongsToMany('App\Models\Produto','tbtransacao_tbproduto','cdTransacao','cdProduto')->withPivot('qtd','valor');
    }



}

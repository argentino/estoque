<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnidadeMedida extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbunidademedida';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cdUnidadeMedida';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'unidadeMedida',
                  'abreviatura'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tbproduto for this model.
     */
    public function produto()
    {
        return $this->hasOne('App\Models\Produto','cdUnidadeMedida','cdUnidadeMedida');
    }



}

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: estoque
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbinstituicao_has_tbproduto`
--

DROP TABLE IF EXISTS `tbinstituicao_has_tbproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbinstituicao_has_tbproduto` (
  `tbInstituicao_cdInstituicao` int(11) NOT NULL,
  `tbProduto_cdProduto` int(11) NOT NULL,
  PRIMARY KEY (`tbInstituicao_cdInstituicao`,`tbProduto_cdProduto`),
  KEY `fk_tbInstituicao_has_tbProduto_tbProduto1_idx` (`tbProduto_cdProduto`),
  CONSTRAINT `fk_tbInstituicao_has_tbProduto_tbProduto1` FOREIGN KEY (`tbProduto_cdProduto`) REFERENCES `tbproduto` (`cdProduto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinstituicao_has_tbproduto`
--

LOCK TABLES `tbinstituicao_has_tbproduto` WRITE;
/*!40000 ALTER TABLE `tbinstituicao_has_tbproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinstituicao_has_tbproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproduto`
--

DROP TABLE IF EXISTS `tbproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbproduto` (
  `cdProduto` int(11) NOT NULL AUTO_INCREMENT,
  `nmProduto` varchar(45) NOT NULL,
  `qtdEstoque` double NOT NULL DEFAULT '0',
  `cdTipo` int(11) NOT NULL,
  `cdUnidadeMedida` int(11) NOT NULL,
  `qtdMinima` double NOT NULL,
  PRIMARY KEY (`cdProduto`),
  KEY `fk_tbProduto_tbTipo_idx` (`cdTipo`),
  KEY `fk_tbProduto_tbUnidadeMedida1_idx` (`cdUnidadeMedida`),
  CONSTRAINT `fk_tbProduto_tbTipo` FOREIGN KEY (`cdTipo`) REFERENCES `tbtipo` (`cdTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbProduto_tbUnidadeMedida1` FOREIGN KEY (`cdUnidadeMedida`) REFERENCES `tbunidademedida` (`cdUnidadeMedida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproduto`
--

LOCK TABLES `tbproduto` WRITE;
/*!40000 ALTER TABLE `tbproduto` DISABLE KEYS */;
INSERT INTO `tbproduto` VALUES (1,'Leite',0,1,1,0),(2,'Arroz',0,2,2,0),(4,'Água mineral',0,1,1,50);
/*!40000 ALTER TABLE `tbproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtipo`
--

DROP TABLE IF EXISTS `tbtipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtipo` (
  `cdTipo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  PRIMARY KEY (`cdTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtipo`
--

LOCK TABLES `tbtipo` WRITE;
/*!40000 ALTER TABLE `tbtipo` DISABLE KEYS */;
INSERT INTO `tbtipo` VALUES (1,'Perecível'),(2,'Integral');
/*!40000 ALTER TABLE `tbtipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtransacao`
--

DROP TABLE IF EXISTS `tbtransacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtransacao` (
  `cdTransacao` int(11) NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `valorTotal` double NOT NULL DEFAULT '0',
  `eDoacao` tinyint(2) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`cdTransacao`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtransacao`
--

LOCK TABLES `tbtransacao` WRITE;
/*!40000 ALTER TABLE `tbtransacao` DISABLE KEYS */;
INSERT INTO `tbtransacao` VALUES (10,'2018-10-25 00:00:00',6,0,'entrada'),(11,'2018-10-25 00:00:00',93,0,'entrada');
/*!40000 ALTER TABLE `tbtransacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtransacao_tbproduto`
--

DROP TABLE IF EXISTS `tbtransacao_tbproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtransacao_tbproduto` (
  `cdTransacao` int(11) NOT NULL,
  `cdProduto` int(11) NOT NULL,
  `qtd` double NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`cdTransacao`,`cdProduto`),
  KEY `fk_tbTransacao_has_tbProduto_tbProduto1_idx` (`cdProduto`),
  KEY `fk_tbTransacao_has_tbProduto_tbTransacao1_idx` (`cdTransacao`),
  CONSTRAINT `fk_tbTransacao_has_tbProduto_tbProduto1` FOREIGN KEY (`cdProduto`) REFERENCES `tbproduto` (`cdProduto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbTransacao_has_tbProduto_tbTransacao1` FOREIGN KEY (`cdTransacao`) REFERENCES `tbtransacao` (`cdTransacao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtransacao_tbproduto`
--

LOCK TABLES `tbtransacao_tbproduto` WRITE;
/*!40000 ALTER TABLE `tbtransacao_tbproduto` DISABLE KEYS */;
INSERT INTO `tbtransacao_tbproduto` VALUES (10,1,2,3),(11,1,1,3),(11,2,45,2);
/*!40000 ALTER TABLE `tbtransacao_tbproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbunidademedida`
--

DROP TABLE IF EXISTS `tbunidademedida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbunidademedida` (
  `cdUnidadeMedida` int(11) NOT NULL AUTO_INCREMENT,
  `unidadeMedida` varchar(45) NOT NULL,
  PRIMARY KEY (`cdUnidadeMedida`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbunidademedida`
--

LOCK TABLES `tbunidademedida` WRITE;
/*!40000 ALTER TABLE `tbunidademedida` DISABLE KEYS */;
INSERT INTO `tbunidademedida` VALUES (1,'L'),(2,'Pacote');
/*!40000 ALTER TABLE `tbunidademedida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Filipe Argentino Domingues Alves','filipeadominguesa@gmail.com','$2y$10$ZVqnf1Gj6J2nkyp32.lTd.Iw0C2iqQ14SqgTFX/.S9F1.tXvw7cwG','pR8wTzmdV8CvQPFyBjXE4KDRXCOSfbFfbo0NplPXWFUB2nhnWqqr1oHJdOK5','2018-10-18 19:16:27','2018-09-28 20:45:26'),(2,'José do Uber','uber@uber.com','$2y$10$ZkVlCE5AFKfwIyZHUx9coeLs0guMjctQmM.rHrV6VtcwhF4BdQ1hW','9iD6P0n8Hh3yS7oLABI17KoFiw6HYBrCcm6Zyj5enB5hBvU7LCqOjsb0HCcB','2018-10-05 19:26:53','2018-10-05 22:26:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-25 10:44:57

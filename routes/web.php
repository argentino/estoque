<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@inicio')->middleware('auth');

Route::fallback(function(){ return response()->view('errors.404', [], 404); });

Route::group(
[
    'prefix' => 'produtos',
    'middleware' => ['auth']
], function () {

    Route::get('/', 'ProdutosController@index')
         ->name('produtos.produto.index');

    Route::get('/create','ProdutosController@create')
         ->name('produtos.produto.create');

    Route::get('/show/{produto}','ProdutosController@show')
         ->name('produtos.produto.show')
         ->where('id', '[0-9]+');

    Route::get('/{produto}/edit','ProdutosController@edit')
         ->name('produtos.produto.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'ProdutosController@store')
         ->name('produtos.produto.store');
               
    Route::put('produto/{produto}', 'ProdutosController@update')
         ->name('produtos.produto.update')
         ->where('id', '[0-9]+');

    Route::delete('/produto/{produto}','ProdutosController@destroy')
         ->name('produtos.produto.destroy')
         ->where('id', '[0-9]+');

     Route::post('/sessao', 'ProdutosController@sessao')
         ->name('produtos.produto.sessao');

});

Route::group(
[
    'prefix' => 'tipos',
    'middleware' => ['auth']
], function () {

    Route::get('/', 'TiposController@index')
         ->name('tipos.tipo.index');

    Route::get('/create','TiposController@create')
         ->name('tipos.tipo.create');

    Route::get('/show/{tipo}','TiposController@show')
         ->name('tipos.tipo.show')
         ->where('id', '[0-9]+');

    Route::get('/{tipo}/edit','TiposController@edit')
         ->name('tipos.tipo.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'TiposController@store')
         ->name('tipos.tipo.store');
               
    Route::put('tipo/{tipo}', 'TiposController@update')
         ->name('tipos.tipo.update')
         ->where('id', '[0-9]+');

    Route::delete('/tipo/{tipo}','TiposController@destroy')
         ->name('tipos.tipo.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'unidade_medidas',
    'middleware' => ['auth']
], function () {

    Route::get('/', 'UnidadeMedidasController@index')
         ->name('unidade_medidas.unidade_medida.index');

    Route::get('/create','UnidadeMedidasController@create')
         ->name('unidade_medidas.unidade_medida.create');

    Route::get('/show/{unidadeMedida}','UnidadeMedidasController@show')
         ->name('unidade_medidas.unidade_medida.show')
         ->where('id', '[0-9]+');

    Route::get('/{unidadeMedida}/edit','UnidadeMedidasController@edit')
         ->name('unidade_medidas.unidade_medida.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'UnidadeMedidasController@store')
         ->name('unidade_medidas.unidade_medida.store');
               
    Route::put('unidade_medida/{unidadeMedida}', 'UnidadeMedidasController@update')
         ->name('unidade_medidas.unidade_medida.update')
         ->where('id', '[0-9]+');

    Route::delete('/unidade_medida/{unidadeMedida}','UnidadeMedidasController@destroy')
         ->name('unidade_medidas.unidade_medida.destroy')
         ->where('id', '[0-9]+');

});

Auth::routes();

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(
[
    'prefix' => 'transacao_entradas',
    'middleware' => ['auth']
], function () {

    Route::get('/', 'TransacaoEntradasController@index')
         ->name('transacao_entradas.transacao_entradas.index');

    Route::get('/create','TransacaoEntradasController@create')
         ->name('transacao_entradas.transacao_entradas.create');

    Route::get('/show/{transacaoEntradas}','TransacaoEntradasController@show')
         ->name('transacao_entradas.transacao_entradas.show')
         ->where('id', '[0-9]+');

    Route::get('/{transacaoEntradas}/edit','TransacaoEntradasController@edit')
         ->name('transacao_entradas.transacao_entradas.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'TransacaoEntradasController@store')
         ->name('transacao_entradas.transacao_entradas.store');
               
    Route::put('transacao_entradas/{transacaoEntradas}', 'TransacaoEntradasController@update')
         ->name('transacao_entradas.transacao_entradas.update')
         ->where('id', '[0-9]+');

    Route::delete('/transacao_entradas/{transacaoEntradas}','TransacaoEntradasController@destroy')
         ->name('transacao_entradas.transacao_entradas.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'transacao_saidas',
    'middleware' => ['auth']
], function () {

    Route::get('/', 'TransacaoSaidasController@index')
         ->name('transacao_saidas.transacao_saidas.index');

    Route::get('/create','TransacaoSaidasController@create')
         ->name('transacao_saidas.transacao_saidas.create');

    Route::get('/show/{transacaoSaidas}','TransacaoSaidasController@show')
         ->name('transacao_saidas.transacao_saidas.show')
         ->where('id', '[0-9]+');

    Route::get('/{transacaoSaidas}/edit','TransacaoSaidasController@edit')
         ->name('transacao_saidas.transacao_saidas.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'TransacaoSaidasController@store')
         ->name('transacao_saidas.transacao_saidas.store');
               
    Route::put('transacao_saidas/{transacaoSaidas}', 'TransacaoSaidasController@update')
         ->name('transacao_saidas.transacao_saidas.update')
         ->where('id', '[0-9]+');

    Route::delete('/transacao_saidas/{transacaoSaidas}','TransacaoSaidasController@destroy')
         ->name('transacao_saidas.transacao_saidas.destroy')
         ->where('id', '[0-9]+');

});
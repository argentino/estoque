@extends ('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <center>
                <h1> Oops! Página não encontrada</h1>
                <img id='imghome' style="margin:auto; height:500px;" src="{{asset('img/img.jpg')}}">
                <p style="font-size: 20px"> Desculpe, não queríamos que isso acontecesse!
                Você pode  
                <a href="/">voltar para a página inicial aqui. </a></p>
            </center>
        </div>
    </div>
@endsection
@extends('layouts.App')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Unidades de Medida</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('unidade_medidas.unidade_medida.create') }}" class="btn btn-success" title="Nova unidade de medida">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($unidadeMedidas) == 0)
            <div class="panel-body text-center">
                <h4>Não há unidades de medida cadastradas</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Abreviatura</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($unidadeMedidas as $unidadeMedida)
                        <tr>
                            <td>{{ $unidadeMedida->unidadeMedida }}</td>
                            <td>{{ $unidadeMedida->abreviatura }}</td>
                            <td>

                                <form method="POST" action="{!! route('unidade_medidas.unidade_medida.destroy', $unidadeMedida->cdUnidadeMedida) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-sm pull-right" role="group">
                                        <a href="{{ route('unidade_medidas.unidade_medida.show', $unidadeMedida->cdUnidadeMedida ) }}" class="btn btn-info" title="Mostrar unidade de medida">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('unidade_medidas.unidade_medida.edit', $unidadeMedida->cdUnidadeMedida ) }}" class="btn btn-primary" title="Editar unidade de medida">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Excluir unidade de medida" onclick="return confirm(&quot;Excluir unidade de medida?&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $unidadeMedidas->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection
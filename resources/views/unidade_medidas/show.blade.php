@extends('layouts.App')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Unidade de medida' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('unidade_medidas.unidade_medida.destroy', $unidadeMedida->cdUnidadeMedida) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('unidade_medidas.unidade_medida.index') }}" class="btn btn-primary" title="Mostrar todas unidades de medida">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('unidade_medidas.unidade_medida.create') }}" class="btn btn-success" title="Nova unidade de medida">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('unidade_medidas.unidade_medida.edit', $unidadeMedida->cdUnidadeMedida ) }}" class="btn btn-primary" title="Editar unidade de medida">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Excluir unidade de medida" onclick="return confirm(&quot;Excluir unidade de medida??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nome</dt>
            <dd>{{ $unidadeMedida->unidadeMedida }}</dd>

        </dl>

    </div>
</div>

@endsection
@extends('layouts.App')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 class="mt-5 mb-5">Nova unidade de medida</h4>
            </span>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('unidade_medidas.unidade_medida.store') }}" accept-charset="UTF-8" id="create_unidade_medida_form" name="create_unidade_medida_form" class="form-horizontal">
            {{ csrf_field() }}
             @if(session('novoProduto'))
                <input type="hidden" id="novoProduto" name="novoProduto" value="true">
            @endif
             @if(session('origem')=='produtos')
                <input name="origem" value="produtos" type="hidden">
            @else
                <input name="origem" value="" type="hidden">
            @endif
            @include ('unidade_medidas.form', [
                                        'unidadeMedida' => null,
                                      ])

                <div class="form-group">
                    <div class="pull-right" style="padding-right: 15px">
                        <a class="btn btn-danger" style="max-width: 87.48px;"  
                        @if(session('origem')=='produtos')
                        onclick="session()"
                        {{session()->forget('origem')}}
                        @else
                        href="{{ route('unidade_medidas.unidade_medida.index') }}"
                        @endif
                        >Cancelar</a>
                        <input class="btn btn-primary" type="submit" value="Cadastrar">
                    </div>
                </div>

            </form>

        </div>
    </div>
    
    <form id="session" action="{{route('produtos.produto.sessao')}}" method="POST">
        {{ csrf_field() }}
            @if(session('novoProduto'))
                {{session()->forget('novoProduto')}}
                <input type="hidden" id="novoProduto" name="novoProduto" value="true">
            @endif
            <input type="hidden" id="nmProdutoSession" name="nmProduto">
            <input type="hidden" id="qtdMinimaSession" name="qtdMinima">
            <input type="hidden" id="cdTipoSession" name="cdTipo">
            <input type="hidden" id="cdUnidadeMedidaSession" name="cdUnidadeMedida">
    </form> 

    @section('script')
        <script>
            function session(){
                $('#nmProdutoSession').val('{{session('nome')}}');
                $('#qtdMinimaSession').val('{{session('qtd')}}');
                $('#cdTipoSession').val('{{session('cdTipo')}}');
                $('#cdUnidadeMedidaSession').val('{{session('cdUnidadeMedida')}}');
                $('#session').submit();
            }
        </script>
    @endsection
@endsection



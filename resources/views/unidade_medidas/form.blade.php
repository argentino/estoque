
<div class="form-group {{ $errors->has('unidadeMedida') ? 'has-error' : '' }}">
    <label for="unidadeMedida" class="col-md-2 control-label">Nome</label>
    <div class="col-md-10">
        <input class="form-control" name="unidadeMedida" type="text" id="unidadeMedida" value="{{ old('unidadeMedida', optional($unidadeMedida)->unidadeMedida) }}" minlength="1" maxlength="45" required="true" placeholder="Digite o nome da unidade de medida">
        {!! $errors->first('unidadeMedida', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('abreviatura') ? 'has-error' : '' }}">
    <label for="abreviatura" class="col-md-2 control-label">Abreviatura</label>
    <div class="col-md-10">
        <input class="form-control" name="abreviatura" type="text" id="abreviatura" value="{{ old('unidadeMedida', optional($unidadeMedida)->abreviatura) }}" minlength="1" maxlength="10" required="true" placeholder="Digite a abreviatura">
        {!! $errors->first('abreviatura', '<p class="help-block">:message</p>') !!}
    </div>
</div>


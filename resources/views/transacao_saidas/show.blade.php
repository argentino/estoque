@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'transacao_saidas' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('transacao_saidas.transacao_saidas.destroy', $transacao_saidas->cdTransacao) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('transacao_saidas.transacao_saidas.index') }}" class="btn btn-primary" title="Show All transacao_saidas">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('transacao_saidas.transacao_saidas.create') }}" class="btn btn-success" title="Create New transacao_saidas">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('transacao_saidas.transacao_saidas.edit', $transacao_saidas->cdTransacao ) }}" class="btn btn-primary" title="Edit transacao_saidas">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete transacao_saidas" onclick="return confirm(&quot;Delete transacao_saidas??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Data</dt>
            <dd>{{ $transacao_saidas->data }}</dd>
            <dt>Valor Total</dt>
            <dd>{{ $transacao_saidas->valorTotal }}</dd>
            <dt>E Doacao</dt>
            <dd>{{ $transacao_saidas->eDoacao }}</dd>

        </dl>

    </div>
</div>

@endsection
@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 class="mt-5 mb-5">Registrar saída</h4>
            </span>

           

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('transacao_saidas.transacao_saidas.store') }}" accept-charset="UTF-8" id="create_transacao_saidas_form" name="create_transacao_saidas_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('transacao_saidas.form', [
                                        'transacao_saidas' => null,
                                        'produtos' => $produtos
                                      ])

                <div class="btn-group btn-group-sm pull-right" role="group">
                    <div class="col-md-offset-2 col-md-12 ">
                         <a class="btn btn-danger" style="max-width: 87.48px;" href="{{url('/')}}">Cancelar</a>
                        <input class="btn btn-primary" type="submit" value="Cadastrar">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection



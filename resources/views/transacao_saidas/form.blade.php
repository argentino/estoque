@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;  
  }
  .ui-menu-item a{
      text-decoration: none;
      border: none
  }
  .ui-menu-item:hover,.ui-menu-item:hover a{
      background-color:#0489B1;
      color: white;
      border: none
  }
  </style>
@endsection

<div class="form-group {{ $errors->has('data') ? 'has-error' : '' }}">
    <label for="data" class="col-md-2 control-label">Data</label>
    <div class="col-md-10">
        <input class="form-control" name="data" type="date" id="data" value="{{ old('data', optional($transacao_saidas)->data) }}"
            minlength="1" required="true" placeholder="Enter data here...">
        {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<input class="form-control" type="hidden" name="eDoacao" type="text" value="0">
<input type="hidden" class="form-control" name="tipo" type="text" value="saida" required="true">
<input type="hidden" class="form-control" name="valorTotal" type="number" value="0">

<div style="margin-top:100px">
    <span>
        <h4 class="mt-5 mb-5">Saída de produtos</h4>
    </span>
</div>
<div class="col-md-12" style="margin-bottom: 20px">
    <table class="table">
        <thead>
            <th></th>
        </thead>
        <tbody></tbody>
    </table>

    <div class="row">
        <div class="col-md-3 ui-widget" style="padding: 0 15px; margin-bottom: 15px">
            <label for="produtos">Produtos</label>
            <div>
                <select name="produtos" id="produtos">
                    @if($produtos==null)
                    <option selected disabled>Não há produtos</option></select>
                @else
                @foreach ($produtos as $p)
                <option value="{{$p->cdProduto}}">{{$p->nmProduto}}</option>
                @endforeach
                </select>
            </div>
            @foreach ($produtos as $p)
            <input id="tipo{{$p->cdProduto}}" type="hidden" value="{{$p->tipo->descricao}}">
            <input id="unidade{{$p->cdProduto}}" type="hidden" value="{{$p->unidademedida->unidadeMedida}}">
            <input id="quantidade{{$p->cdProduto}}" type="hidden" value="{{$p->qtdEstoque}}">
            @endforeach
            @endif
        </div>
        <div class="col-md-3">
            <label for="qtd">Quantidade</label>
            <input class="form-control col-md-3" name="qtd" type="number" id="qtd" placeholder="Quantidade">
        </div>
        <div class="col-md-3 btn-group btn-group-sm" style="margin-top:2em;" role="group">
            <a href="#" class="btn btn-primary" title="Remover produto" id="add">
                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
            </a>
        </div>
    </div>

    <div class="col-md-12">
        <table class="table">
            <thead>
                <th></th>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <div class="col-md-12" style="margin-top: 50px">
        <table class="table">
            <thead>
                <th>Produto</th>
                <th>Tipo</th>
                <th>Un. Medida</th>
                <th>Quantidade</th>
                <th></th>
            </thead>
            <tbody id="tabela">
            </tbody>
        </table>

        <div class="produtosDiv"></div>
        <div class="quantidadesDiv"></div>

        @section('script')
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $('#add').blur(function () {
                $(".custom-combobox-input").focus();
            });
            //autocompletar
            $(function () {
                $.widget("custom.combobox", {
                    _create: function () {
                        this.wrapper = $("<span>")
                            .addClass("custom-combobox")
                            .insertAfter(this.element);

                        this.element.hide();
                        this._createAutocomplete();
                        this._createShowAllButton();
                    },

                    _createAutocomplete: function () {
                        var selected = this.element.children(":selected"),
                            value = selected.val() ? selected.text() : "";

                        this.input = $("<input>")
                            .appendTo(this.wrapper)
                            .val(value)
                            .attr("title", "")
                            .addClass(
                                "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left"
                            )
                            .autocomplete({
                                delay: 0,
                                minLength: 0,
                                source: $.proxy(this, "_source")
                            })
                            .tooltip({
                                classes: {
                                    "ui-tooltip": "ui-state-highlight"
                                }
                            });

                        this._on(this.input, {
                            autocompleteselect: function (event, ui) {
                                ui.item.option.selected = true;
                                this._trigger("select", event, {
                                    item: ui.item.option
                                });
                            },

                            autocompletechange: "_removeIfInvalid"
                        });
                    },

                    _createShowAllButton: function () {
                        var input = this.input,
                            wasOpen = false;

                        $("<a>")
                            .attr("tabIndex", -1)
                            .attr("title", "Mostrar todos produtos")
                            .tooltip()
                            .appendTo(this.wrapper)
                            .button({
                                icons: {
                                    primary: "ui-icon-triangle-1-s"
                                },
                                text: false
                            })
                            .removeClass("ui-corner-all")
                            .addClass("custom-combobox-toggle ui-corner-right")
                            .on("mousedown", function () {
                                wasOpen = input.autocomplete("widget").is(":visible");
                            })
                            .on("click", function () {
                                input.trigger("focus");

                                // Close if already visible
                                if (wasOpen) {
                                    return;
                                }

                                // Pass empty string as value to search for, displaying all results
                                input.autocomplete("search", "");
                            });
                    },

                    _source: function (request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response(this.element.children("option").map(function () {
                            var text = $(this).text();
                            if (this.value && (!request.term || matcher.test(text)))
                                return {
                                    label: text,
                                    value: text,
                                    option: this
                                };
                        }));
                    },

                    _removeIfInvalid: function (event, ui) {

                        // Selected an item, nothing to do
                        if (ui.item) {
                            return;
                        }

                        // Search for a match (case-insensitive)
                        var value = this.input.val(),
                            valueLowerCase = value.toLowerCase(),
                            valid = false;
                        this.element.children("option").each(function () {
                            if ($(this).text().toLowerCase() === valueLowerCase) {
                                this.selected = valid = true;
                                return false;
                            }
                        });

                        // Found a match, nothing to do
                        if (valid) {
                            return;
                        }

                        // Remove invalid value
                        this.input
                            .val("")
                            .attr("title", value + " não corresponde a nenhum produto")
                            .tooltip("open");
                        this.element.val("");
                        this._delay(function () {
                            this.input.tooltip("close").attr("title", "");
                        }, 2500);
                        this.input.autocomplete("instance").term = "";
                    },

                    _destroy: function () {
                        this.wrapper.remove();
                        this.element.show();
                    }
                });

                $("#produtos").combobox();
            });
            //fim do autocompletar
            var produtos = new Array();
            var quantidades = new Array();
            var somar = false;
            var executar = false;
            $('#add').click(function () {
                if ($('#produtos').val() != null) {
                    id = $('#produtos option:selected').val();
                    if ($('#qtd').val() > 0) {
                        if (parseFloat($('#qtd').val()) <= parseFloat($('#quantidade' + id).val()))  { 
                            executar = true;  
                        } else {
                            if(confirm('Estoque não possui essa quantidade produtos para retirar, deseja retirar mesmo assim?')){
                                executar = true;  
                            } //permitir retirar mais do que o estoque permite, porém exibir aviso
                        }
                        if (executar){
                            for (i=0 ; i<produtos.length ;i++){
                                if(id == produtos[i]){
                                    if(confirm("Esse produto já foi adicionado, deseja adicioná-lo novamente?")){
                                        $('#qtd'+id).text(parseFloat(quantidades[i])+parseFloat($('#qtd').val()));
                                    quantidades[i]=parseFloat(quantidades[i])+parseFloat($('#qtd').val());
                                    qtd = $('#qtd').val();
                                    $('#quantidade' + id).val($('#quantidade' + id).val()-parseFloat(qtd));
                                    }  
                                    somar=true;
                                }
                            }
                            if (!somar){
                            nome = $('#produtos option:selected').text();
                            qtd = $('#qtd').val();
                            tipo = $('#tipo' + id).val();
                            unidade = $('#unidade' + id).val();
                            $('#quantidade' + id).val($('#quantidade' + id).val()-qtd);
                            $('#tabela').append('<tr id=' + id + '><td>' + nome + '</td><td>' + tipo +
                                '</td><td>' + unidade + '</td><td id="qtd'+id+'">' + qtd +
                                '</td><td> <btn onclick="remove(' + id +
                                ')" class="btn btn-danger" title="Remover produto"><span class="glyphicon glyphicon-minus" aria-hidden="true"></a> </td></tr>'
                            );
                            produtos.push(id);
                            quantidades.push(qtd);
                            }
                            somar=false;
                        }
                    } else {
                        alert('Quantidade inválida');
                    }
                } else {
                    alert('Selecione um produto');
                }

            });

            function remove(id) {
                $('#' + id).remove();
                i = produtos.findIndex(function (produto) {
                    return produto == id;
                });
                produtos[i] = produtos[produtos.length - 1];
                produtos.pop();
                $('#quantidade' + id).val(parseFloat($('#quantidade' + id).val()) + parseFloat(quantidades[i]));
                quantidades[i] = quantidades[quantidades.length - 1];
                quantidades.pop();
            }

            $('form').submit(function () {
                $('.produtosDiv').empty();
                $('.quantidadesDiv').empty();
                for (i = 0; i < produtos.length; i++) {
                    $('.produtosDiv').append("<input type='hidden' name='produtos[]' value='" + produtos[i] +
                        "'/>");
                    $('.quantidadesDiv').append("<input type='hidden' name='quantidades[]' value='" +
                        quantidades[i] + "'/>");
                };
            });

        </script>
        @endsection

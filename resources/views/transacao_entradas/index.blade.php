@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Entrada de Produtos</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('transacao_entradas.transacao_entradas.create') }}" class="btn btn-success" title="Create New transacao_entradas">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($transacao_entradas) == 0)
            <div class="panel-body text-center">
                <h4>No transacao_entradas Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Valor Total</th>
                            <th>E Doacao</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($transacao_entradas as $transacao_entradas)
                        <tr>
                            <td>{{ $transacao_entradas->data }}</td>
                            <td>{{ $transacao_entradas->valorTotal }}</td>
                            <td>{{ $transacao_entradas->eDoacao }}</td>

                            <td>

                                <form method="POST" action="{!! route('transacao_entradas.transacao_entradas.destroy', $transacao_entradas->cdTransacao) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('transacao_entradas.transacao_entradas.show', $transacao_entradas->cdTransacao ) }}" class="btn btn-info" title="Show transacao_entradas">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('transacao_entradas.transacao_entradas.edit', $transacao_entradas->cdTransacao ) }}" class="btn btn-primary" title="Edit transacao_entradas">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete transacao_entradas" onclick="return confirm(&quot;Delete transacao_entradas?&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $transacao_entradas->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection
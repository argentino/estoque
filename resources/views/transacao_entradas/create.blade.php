@extends('layouts.app')

@section('content')

<div class="panel panel-default">

    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">Registrar entrada</h4>
        </span>



    </div>

    <div class="panel-body">

        @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif

        <form method="POST" action="{{ route('transacao_entradas.transacao_entradas.store') }}" accept-charset="UTF-8"
            id="create_transacaoentrada_form" name="create_transacaoentrada_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('transacao_entradas.form', [
            'transacao_entradas' => null,
            'produtos' => $produtos
            ])
            <div class="btn-group btn-group-sm pull-right" role="group" style="clear:both">
                <div class="col-md-12 ">
                    <a class="btn btn-danger" style="max-width: 87.48px;" href="{{url('/')}}">Cancelar</a>
                    <input class="btn btn-primary" type="submit" value="Cadastrar">
                </div>
            </div>

        </form>
@yield('form')
    </div>

</div>
@endsection

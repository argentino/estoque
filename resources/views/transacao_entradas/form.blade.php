@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
    
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;  
    width:230px;
  }
  .ui-menu-item a{
      text-decoration: none;
      border: none
  }
  .ui-menu-item:hover,.ui-menu-item:hover a{
      background-color:#0489B1;
      color: white;
      border: none
  }
  </style>
@endsection

<div class="form-group {{ $errors->has('data') ? 'has-error' : '' }}">
    <label for="data" class="col-md-2 control-label">Data</label>
    <div class="col-md-10">
        <input class="form-control" name="data" type="date" id="data" value="@if(session('data')){{session('data')}}{{session()->forget('data')}}@else {{ old('data', optional($transacao_entradas)->data) }} @endif"
            minlength="1" required="true" placeholder="Enter data here...">
        {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('eDoacao') ? 'has-error' : '' }}">
    <label for="eDoacao" class="col-md-2 control-label">Tipo</label>
    <div class="col-md-10">
        <input class="form-control" type="hidden" name="eDoacao" type="text" id="inputDoacao" value="{{ old('eDoacao', optional($transacao_entradas)->eDoacao) }}" minlength="1" required="true" placeholder="Enter e doacao here...">
        <select class="form-control" id="eDoacao" name="eDoacao" onchange="exibir_ocultar()" required>
            <option selected hidden disabled>Selecione a forma de entrada</option>          
            <option @if (session('tipoEntrada')=='1') selected @endif value="1">Doação</option>
            <option @if (session('tipoEntrada')=='0') selected @endif value="0">Compra</option>
        </select>
    </div>
</div>


<div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
    <div class="col-md-10">
        <input type="hidden" value="entrada" class="form-control" name="tipo" type="text" id="tipo" value="{{ old('tipo', optional($transacao_entradas)->tipo) }}" minlength="1" maxlength="45" required="true">
        {!! $errors->first('tipo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div style="margin-top:50px"> 
<span>
    <h4 class="mt-5 mb-5">Entrada de produtos</h4>
</span>
</div>
<div class="col-md-12" style="margin-bottom: 20px">
    <table class="table">
        <thead>
            <th></th>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-3 col-sm-10 col-xs-10 ui-widget" style="padding: 0 15px; margin-bottom: 15px">
        <label for="produtos">Produtos</label>
        <div>
            <select name="produtos" id="produtos" >
                @if($produtos==null)
                <option selected disabled>Não há produtos</option></select>
            @else
            @foreach ($produtos as $p)
            <option value="{{$p->cdProduto}}">{{$p->nmProduto}}</option>
            @endforeach
            </select>
            @foreach ($produtos as $p)
            <input id="tipo{{$p->cdProduto}}" type="hidden" value="{{$p->tipo->descricao}}">
            <input id="unidade{{$p->cdProduto}}" type="hidden" value="{{$p->unidademedida->unidadeMedida}}">
            @endforeach
            @endif
        </div>  
    </div>
    
    <div class="col-md-1 row btn-group-sm" role="group" style="margin-top:25px;">
        <a class="btn btn-success" title="Criar novo produto" onclick="session()">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
         </a>
    </div>

    <div class="col-md-3">
        <label for="qtd">Quantidade</label>
        <input name="qtd" class="form-control" type="number" id="qtd" placeholder="Quantidade">
    </div>
    <div class="col-md-3     esconder">
        <label for="preco">Valor unitário</label>
        <input name="preco" class="form-control" type="number" id="preco" placeholder="Valor und">
    </div>
    <div class="col-md-1 btn-group btn-group-sm" role="group">
            <a id="add" style="margin-top: 2.2em" href="#" class="btn btn-primary" title="Adicionar produto">
                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
            </a>
    </div>
</div>

<div class="col-md-12" style="margin-top: 50px">
    <table class="table">
        <thead>
            <th>Produto</th>
            <th>Tipo</th>
            <th>Un. Medida</th>
            <th id="thPreco">Preço</th>
            <th>Quantidade</th>
            <th id="thPrecoTotal">Preço Total</th>
            <th></th>
        </thead>
        <tbody id="tabela">
        </tbody>
    </table>
</div>

<div class="col-md-12">
    <table class="table">
        <thead>
            <th></th>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div style="float:right;" class="form-group {{ $errors->has('valorTotal') ? 'has-error' : '' }}">
    <label for="valorTotal" id="lbDoacao" class="col-md-4 control-label">Valor Total</label>
    <div class="col-md-8 ">
        <input class="form-control" readonly name="valorTotal" type="number" id="valorTotal" value="{{ old('valorTotal', optional($transacao_entradas)->valorTotal) }}"
            min="0" required="true" placeholder="Valor total da transação">
        {!! $errors->first('valorTotal', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="produtosDiv"></div>
<div class="precosDiv"></div>
<div class="quantidadesDiv"></div>

@section('form')
<form id="session" action="{{route('produtos.produto.sessao')}}" method="POST">
{{ csrf_field() }}
<input type="hidden" id="destino" name="destino" value="transacao">
<input type="hidden" id="tipoEntrada" name="tipoEntrada">
<input type="hidden" id="dataSession" name="dataSession">
<div id="inputs"></div>
</form>
@endsection

@section('script')
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
    $('#add').blur(function(){
        $(".custom-combobox-input").focus();
    });
    //autocompletar
    $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Mostrar todos produtos" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " não corresponde a nenhum produto" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#produtos" ).combobox();
  } );
  //fim do autocompletar
    function exibir_ocultar() {
        var val = $("#eDoacao").val();
        var preco = document.getElementById('preco');
        var valorTotal = document.getElementById('valorTotal');
        var lbDoacao = document.getElementById('lbDoacao');
        var thPreco = document.getElementById('thPreco');
        var thPrecoTotal = document.getElementById('thPrecoTotal');
        if (val == 0) {
            $(preco).show();
            $(valorTotal).attr('type', 'number');
            $(lbDoacao).show();
            $(thPreco).show();
            $(thPrecoTotal).show();
            $('.esconder').show();

        } else {
            $(preco).hide();
            preco.value = 0;
            valorTotal.value = 0;
            $(valorTotal).attr('type', 'hidden');
            $(lbDoacao).hide();
            $(thPreco).hide();
            $(thPrecoTotal).hide();
            $('.esconder').hide();
        }
    };

    var produtos = new Array();
    var quantidades = new Array();
    var precos = new Array();
    var total = 0;
    $('#add').click(function () {
        if ($("#eDoacao").val() != null) {
            if ($('#produtos').val() != null) {
                if ($('#qtd').val() > 0) {
                    if ($('#preco').val() >= 0) {
                        $("#eDoacao").attr('disabled', 'true');
                        id = $('#produtos option:selected').val();
                        nome = $('#produtos option:selected').text();
                        preco = $('#preco').val();
                        qtd = $('#qtd').val();
                        tipo = $('#tipo' + id).val();
                        unidade = $('#unidade' + id).val();
                        if ($("#eDoacao").val() == 0)
                            $('#tabela').append('<tr id=' + id + '><td>' + nome + '</td><td>' +
                                tipo + '</td><td>' + unidade + '</td><td class="esconder">R$' +
                                preco + '</td><td>' + qtd + '</td> <td class="esconder">R$' + qtd *
                                preco + '</td><td> <btn onclick="remove(' + id +
                                ')" class="btn btn-danger" title="Remover produto"><span class="glyphicon glyphicon-minus" aria-hidden="true"></a> </td></tr>'
                            );
                        else {
                            preco = 0;
                            $('#tabela').append('<tr id=' + id + '><td>' + nome + '</td><td>' +
                                tipo + '</td><td>' + unidade + '</td><td class="hide">R$' +
                                preco + '</td><td>' + qtd + '</td> <td class="hide">R$' + qtd *
                                preco + '</td><td> <btn onclick="remove(' + id +
                                ')" class="btn btn-danger" title="Remover produto"><span class="glyphicon glyphicon-minus" aria-hidden="true"></a> </td></tr>'
                            );
                        }
                        produtos.push(id);
                        quantidades.push(qtd);
                        precos.push(preco);
                        total = total + (preco * qtd);
                        escreverTotal();
                    } else {
                        alert('Preço inválido');
                    }
                } else {
                    alert('Quantidade inválida');
                }
            } else {
                alert('Selecione um produto');
            }
        } else {
            alert('Selecione um tipo de entrada');
        }

    });

    function remove(id) {
        $('#' + id).remove();
        i = produtos.findIndex(function (produto) {
            return produto == id;
        });
        produtos[i] = produtos[produtos.length - 1];
        produtos.pop();
        total = total - (precos[i] * quantidades[i]);
        precos[i] = precos[precos.length - 1];
        precos.pop();
        quantidades[i] = quantidades[quantidades.length - 1];
        quantidades.pop();
        escreverTotal();
    }

    function escreverTotal() {
        if (total != 0)
            $('#valorTotal').val(total);
        else
            $('#valorTotal').val(0);
    }

    $('form').submit(function(){
        $('#inputDoacao').val($('#eDoacao').val());
        $('.produtosDiv').empty();
        $('.precosDiv').empty();
        $('.quantidadesDiv').empty();
        for (i = 0; i < produtos.length; i++) { 
            $('.produtosDiv').append("<input type='hidden' name='produtos[]' value='"+produtos[i]+"'/>");
            $('.precosDiv').append("<input type='hidden' name='precos[]' value='"+precos[i]+"'/>");
            $('.quantidadesDiv').append("<input type='hidden' name='quantidades[]' value='"+quantidades[i]+"'/>");
        }; 
    }); 

    function session(){
        for(i = 0; i < produtos.length; i++){
            $('#inputs').append('<input type="hidden" name="quantidades[]" value="'+quantidades[i]+'"><input type="hidden" name="precos[]"" value="'+precos[i]+'"/><input type="hidden" name="produtos[]" value="'+produtos[i]+'">');
        }
        $('#tipoEntrada').val($('#eDoacao').val());
        $('#dataSession').val($('#data').val());
        $('#session').submit();
    }
</script>
@if(session('tipoEntrada')=="0" || session('tipoEntrada')=="1")
<script>var tipoEntrada = {{session('tipoEntrada')}};exibir_ocultar();</script>
{{session()->forget('tipoEntrada')}}
    @if(session('produtos'))
        <script>         
            $("#eDoacao").attr('disabled', 'true');
            @foreach(session('produtos') as $i => $p)
                id = {{$p}};
                qtd = {{session('quantidades')[$i]}};
                tipo = $('#tipo' + {{$p}}).val();
                unidade = $('#unidade' + {{$p}}).val();
                @foreach($produtos as $produto)
                    @if($produto->cdProduto == $p)
                    nome = "{{$produto->nmProduto}}";
                    @endif
                @endforeach
                if (tipoEntrada==0){
                    preco = {{session('precos')[$i]}};
                    $('#tabela').append('<tr id=' + id + '><td>' + nome + '</td><td>' +
                            tipo + '</td><td>' + unidade + '</td><td class="esconder">R$' +
                            preco + '</td><td>' + qtd + '</td> <td class="esconder">R$' + qtd *
                            preco + '</td><td> <btn onclick="remove(' + id +
                            ')" class="btn btn-danger" title="Remover produto"><span class="glyphicon glyphicon-minus" aria-hidden="true"></a> </td></tr>'
                        );
                } else {
                    preco = 0;
                    $('#tabela').append('<tr id=' + id + '><td>' + nome + '</td><td>' +
                        tipo + '</td><td>' + unidade + '</td><td class="hide">R$' +
                        preco + '</td><td>' + qtd + '</td> <td class="hide">R$' + qtd *
                        preco + '</td><td> <btn onclick="remove(' + id +
                        ')" class="btn btn-danger" title="Remover produto"><span class="glyphicon glyphicon-minus" aria-hidden="true"></a> </td></tr>'
                    );
                }
                produtos.push(id);
                quantidades.push(qtd);
                precos.push(preco);
                total = total + (preco * qtd);
                escreverTotal();
                </script>
            @endforeach
        </script>
        {{session()->forget('produtos')}}
        {{session()->forget('quantidades')}}
        @if(session('precos'))
        {{session()->forget('precos')}}
        @endif
    @endif
@endif
@endsection


@extends('layouts.App')
@section('content')
<div class="container">
    <div class="row justify-content-center  ">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        @if(Auth::check())
       
        <div class="col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading"><i class="glyphicon glyphicon-transfer"></i> Produtos em falta<i class="glyphicon glyphicon-print" style="float: right"></i></div>
                <!-- <div class="input-group" style="margin-top: 3px; margin-left: 3px;">
                    <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-search"></i></span>
                    <input type="text" id="date" class="form-control">
                    <button type="button" class="btn btn-default" style="margin-left: 3px">Pesquisar</button>
                </div> -->
                <div style="height:335px; overflow-y: scroll;">
                    @if(count($produtos) == 0)
                    <div class="panel-body text-center">
                        <h4>Nenhum produto em falta!</h4>
                    </div>
                    @else
                    <div class="panel-body panel-body-with-table">
                        <div class="table-responsive">

                            <table class="table table-striped ">
                                <thead>
                                    <th>Nome</th>
                                    <th>Qtd. em estoque</th>
                                </thead>
                                <tbody>
                                    @foreach($produtos as $produto)
                                    @if( $produto->qtdEstoque < $produto->qtdMinima)
                                        @if( $produto->qtdEstoque > 0)
                                        <tr>
                                            <td>{{ $produto->nmProduto }}</td>
                                            <td>{{ $produto->qtdEstoque }} {{ $produto->unidademedida->abreviatura }}</td>
                                        </tr>
                                        @else
                                        <tr class="danger">
                                            <td>{{ $produto->nmProduto }}</td>
                                            <td>0 {{$produto->unidademedida->abreviatura}}</td>
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-transfer"></i> Estoque<i class="glyphicon glyphicon-print" style="float: right"></i></div>
                <div class="input-group" style="margin-top: 3px; margin-left: 3px;">
                    <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-search"></i></span>
                    <input type="text" id="date" class="form-control">
                    <button type="button" class="btn btn-default" style="margin-left: 3px">Pesquisar</button>
                </div>
                <div style="height:300px; overflow-y: scroll;">

                    @if(count($produtos) == 0)
                    <div class="panel-body text-center">
                        <h4>Nenhum produto disponível!</h4>
                    </div>
                    @else
                    <div class="panel-body panel-body-with-table">
                        <div class="table-responsive">

                            <table class="table table-striped ">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Qtd. em estoque</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($produtos as $produto)
                                    <tr>
                                        <td>{{ $produto->nmProduto }}</td>  
                                        <td>{{ $produto->qtdEstoque }} {{
                                            optional($produto->unidademedida)->abreviatura }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="glyphicon glyphicon-transfer"></i> Últimas transações<i class="glyphicon glyphicon-print" style="float: right"></i></div>
                <div class="panel-body">
                    <!-- <div class="row">
                        <div class="col-md-4">
                            <span>Data inicial</span>
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
                                <input type="text" id="date" class="date form-control">
                            </div>
                        </div </div> <div class="col-md-4">
                        <span>Data final</span>
                        <div class="input-group">
                            <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
                            <input type="text" id="date" class="date form-control">
                        </div>
                    </div>
                    <div class="col-md-2 checkbox">
                            <br>
                            <label>
                            <input name="doacao" type="checkbox">Apenas doações</label>
                        </div> 
                    <div class="col-md-2">
                        <br>
                        <button type="button" class="btn btn-default">Filtrar</button>
                    </div> -->

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="	border-top: 1px dashed #8c8b8b;"> -->
                        </div>
                        <!-- <div class="col-md-12">
                            <div class="col-md-3" style="width: 25%">
                                @foreach($transacoes as $transacao)
                                @foreach($transacao->produto as $p)
                                @if($transacao->tipo == 'saida')
                                <p class="text-left"><i class="glyphicon glyphicon-minus" style="color: red"></i>
                                    {{date('d/m/Y', strtotime($transacao->data))}} - {{$p->pivot->qtd}} <a href="{{url('/unidade_medidas/show/1')}}">{{
                                        optional($p->unidademedida)->abreviatura }}</a> de <a href="{{url('/produtos/show/1')}}">{{$p->nmProduto}}</a></p>
                                @endif
                                @endforeach
                                @endforeach
                            </div>
                            <div class="col-md-3" style="width: 25%">
                                @foreach($transacoes as $transacao)
                                @foreach($transacao->produto as $p)
                                @if($transacao->tipo == 'entrada')
                                </i>
                                    {{date('d/m/Y', strtotime($transacao->data))}} - {{$p->pivot->qtd}} <a href="{{url('/unidade_medidas/show/1')}}">{{
                                        optional($p->unidademedida)->abreviatura }}</a> de <a href="{{url('/produtos/show/1')}}">{{$p->nmProduto}}</a></p>
                                @endif
                                @endforeach
                                @endforeach
                            </div> -->
                            <div class="col-md-12" style="margin: 0 auto; width: 100%">
                            <table class="table table-striped ">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Data</th>
                                        <th>Tipo</th>
                                        <th>Produto</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transacoes as $transacao)
                                    @foreach($transacao->produto as $p)
                                    <tr>
                                        <td>{{ $transacao->cdTransacao   }}</td>
                                        <td>{{date('d/m/Y', strtotime($transacao->data))}}</td>  
                                        @if($transacao->tipo == 'entrada')
                                        <td><p class="text-left"><i title="Entrada" class="glyphicon glyphicon-plus" style="color: green"></td>
                                        @else
                                        <td><p class="text-left"><i title="Saída" class="glyphicon glyphicon-minus" style="color: red"></td>
                                        @endif                                        
                                        <td>{{$p->pivot->qtd}}{{
                                        optional($p->unidademedida)->abreviatura }} de {{$p->nmProduto}}</a></td>

                                    </tr>
                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                            <div style="margin: 0 auto; width: 50%">
                    {{$transacoes->render()}}
                    </div>

                            </div>
                            <!-- <p class="text-center"><i class="glyphicon glyphicon-plus" style="color: green"></i>
                            27/09/2018 - 25 <a href="{{url('/unidade_medidas/show/1')}}">L</a> de <a href="{{url('/produtos/show/1')}}">Leite</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
            @else
            @include('auth.login')
            @endif
        </div>
    </div>
    @endsection

    @section('script')
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.date').datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'pt-br'
            });
        });

    </script>
    @endsection

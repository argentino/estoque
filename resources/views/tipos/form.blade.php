
<div class="form-group {{ $errors->has('descricao') ? 'has-error' : '' }}">
    <label for="descricao" class="col-md-2 control-label">Descrição</label>
    <div class="col-md-10">
        <input class="form-control" name="descricao" type="text" id="descricao" value="{{ old('descricao', optional($tipo)->descricao) }}" minlength="1" maxlength="45" required="true" placeholder="Digite a descrição do tipo de produto">
        {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
    </div>
</div>

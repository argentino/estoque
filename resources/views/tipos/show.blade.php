@extends('layouts.App')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Tipo' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tipos.tipo.destroy', $tipo->cdTipo) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tipos.tipo.index') }}" class="btn btn-primary" title="Mostrar todos os tipos de produto">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tipos.tipo.create') }}" class="btn btn-success" title="Novo tipo de produto">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tipos.tipo.edit', $tipo->cdTipo ) }}" class="btn btn-primary" title="Editar tipo de produto">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Excluir tipo de produto" onclick="return confirm(&quot;Excluir tipo de produto??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Descricao</dt>
            <dd>{{ $tipo->descricao }}</dd>

        </dl>

    </div>
</div>

@endsection
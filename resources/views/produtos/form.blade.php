<div class="form-group {{ $errors->has('nmProduto') ? 'has-error' : '' }}">
    <label for="nmProduto" class="col-md-2 control-label">Nome</label>
    <div class="col-md-10">
        <input class="form-control" name="nmProduto" type="text" id="nmProduto" value="@if(session('nome')){{session('nome')}}{{session()->forget('nome')}}@endif{{ old('nmProduto', optional($produto)->nmProduto) }}" minlength="1" maxlength="45" required="true" placeholder="Digite o nome do produto">
        {!! $errors->first('nmProduto', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('qtdMinima') ? 'has-error' : '' }}">
    <label for="qtdMinima" class="col-md-2 control-label">Quantidade Minima</label>
    <div class="col-md-10">
        <input class="form-control" name="qtdMinima" type="number" id="qtdMinima" value="@if(session('qtd')){{session('qtd')}}{{session()->forget('qtd')}}@endif{{ old('qtdMinima', optional($produto)->qtdMinima) }}" required="true" placeholder="Digite a quantidade mínima no estoque deste produto">
        {!! $errors->first('qtdMinima', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if($produto!=null)
        <input class="form-control" name="qtdEstoque" type="hidden" id="qtdEstoque" value="{{ old('qtdEstoque', optional($produto)->qtdEstoque) }}"  required="true" placeholder="Digite a quantidade em estoque">
        {!! $errors->first('qtdEstoque', '<p class="help-block">:message</p>') !!}
@else
    <input type="hidden" class="form-control" name="qtdEstoque" value="0">
@endif
<div class="form-group {{ $errors->has('cdTipo') ? 'has-error' : '' }}">
    <label for="cdTipo" class="col-md-2 control-label">Tipo</label>
    <div class="col-md-4">
        <select class="col form-control" id="cdTipo" name="cdTipo" required="true">
        	    <option value="" style="display: none;" {{ old('cdTipo', optional($produto)->cdTipo ?: '') == '' ? 'selected' : '' }} disabled selected>Escolha o tipo do produto</option>
        	@foreach ($tbtipos as $key => $tbtipo)
			    <option value="{{ $key }}" 
                @if(session('cdTipo')==$key) selected {{session()->forget('cdTipo')}} @endif
                {{ old('cdTipo', optional($produto)->cdTipo) == $key ? 'selected' : '' }}>
			    	{{ $tbtipo }}
			    </option>
			@endforeach
        </select>
        {!! $errors->first('cdTipo', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col btn-group btn-group-sm" role="group">
        <a class="btn btn-success" title="Criar novo tipo" onclick="session('tipo')">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
         </a>
    </div>
</div>

<div class="form-group {{ $errors->has('cdUnidadeMedida') ? 'has-error' : '' }}">
    <label for="cdUnidadeMedida" class="col-md-2 control-label">Unidade de Medida</label>
    <div class="col-md-4">
        <select class="form-control" id="cdUnidadeMedida" name="cdUnidadeMedida" required="true">
        	    <option value="" style="display: none;" {{ old('cdUnidadeMedida', optional($produto)->cdUnidadeMedida ?: '') == '' ? 'selected' : '' }} disabled selected>Escolha a unidade de medida do produto</option>
        	@foreach ($tbunidademedidas as $key => $tbunidademedida)
			    <option value="{{ $key }}" 
                @if(session('cdUnidadeMedida')==$key) selected {{session()->forget('cdUnidadeMedida')}} @endif
                {{ old('cdUnidadeMedida', optional($produto)->cdUnidadeMedida) == $key ? 'selected' : '' }}>
			    	{{ $tbunidademedida }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('cdUnidadeMedida', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col btn-group btn-group-sm" role="group">
        <a class="btn btn-success" title="Criar nova unidade de medida"  onclick="session('unidadeMedida')">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
         </a>
    </div>
</div>


@section('form')
<form id="session" action="{{route('produtos.produto.sessao')}}" method="POST">
{{ csrf_field() }}
<input type="hidden" id="nmProdutoSession" name="nmProduto">
<input type="hidden" id="qtdMinimaSession" name="qtdMinima">
<input type="hidden" id="cdTipoSession" name="cdTipo">
<input type="hidden" id="cdUnidadeMedidaSession" name="cdUnidadeMedida">
<input type="hidden" id="destino" name="destino">
@if(session('novoProduto'))
    <input type="hidden" id="novoProduto" name="novoProduto" value="true">
@endif
</form>
@endsection

@section('script')
<script>
function session(destino){
  $('#nmProdutoSession').val($('#nmProduto').val());
  $('#qtdMinimaSession').val($('#qtdMinima').val());
  $('#cdTipoSession').val($('#cdTipo').val());
  $('#cdUnidadeMedidaSession').val($('#cdUnidadeMedida').val());
  $('#destino').val(destino);
  $('#session').submit();
}
</script>
@endsection
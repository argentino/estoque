@extends('layouts.App')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Produtos</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('produtos.produto.create') }}" class="btn btn-success" title="Criar novo produto">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($produtos) == 0)
            <div class="panel-body text-center">
                <h4>No Produtos Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="tbproduto" class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Qtd. em estoque</th>
                            <th>Qtd. mínima</th>
                            <th>Tipo</th>
                            <th>Unidade de Medida</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($produtos as $produto)
                        <tr>
                            <td>{{ $produto->nmProduto }}</td>
                            <td>{{ $produto->qtdEstoque }}</td>
                            <td>{{ $produto->qtdMinima }}</td>
                            <td>{{ optional($produto->tipo)->descricao }}</td>
                            <td>{{ optional($produto->unidademedida)->unidadeMedida }}</td>

                            <td>

                                <form method="POST" action="{!! route('produtos.produto.destroy', $produto->cdProduto) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-sm pull-right" role="group">
                                        <a href="{{ route('produtos.produto.show', $produto->cdProduto ) }}" class="btn btn-info" title="Exibir produto">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('produtos.produto.edit', $produto->cdProduto ) }}" class="btn btn-primary" title="Editar produto">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Excluir produto" onclick="return confirm(&quot;Excluir produto?&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $produtos->render() !!}
        </div>
        
        @endif
    
    </div>
    
@section('script')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
$('#tbproduto').DataTable({ 
    language: {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
});
</script>
@endsection
@endsection
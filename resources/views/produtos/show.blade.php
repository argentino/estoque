@extends('layouts.App')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Produto' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('produtos.produto.destroy', $produto->cdProduto) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('produtos.produto.index') }}" class="btn btn-primary" title="Mostrar todos produtos">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('produtos.produto.create') }}" class="btn btn-success" title="Novo produto">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('produtos.produto.edit', $produto->cdProduto ) }}" class="btn btn-primary" title="Editar produto">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Excluir produto" onclick="return confirm(&quot;Excluir produto??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nome</dt>
            <dd>{{ $produto->nmProduto }}</dd>
            <dt>Quantidade em estoque</dt>
            <dd>{{ $produto->qtdEstoque }}</dd>
            <dt>Tipo</dt>
            <dd>{{ optional($produto->tipo)->descricao }}</dd>
            <dt>Unidade de medida</dt>
            <dd>{{ optional($produto->unidademedida)->unidadeMedida }}</dd>

        </dl>

    </div>
</div>

@endsection
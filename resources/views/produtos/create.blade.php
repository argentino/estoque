@extends('layouts.App')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 class="mt-5 mb-5">Novo produto</h4>
            </span>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('produtos.produto.store') }}" accept-charset="UTF-8" id="create_produto_form" name="create_produto_form" class="form-horizontal">
            {{ csrf_field() }}
                @include ('produtos.form', [
                                        'produto' => null,
                                      ])
            <div class="form-group">
                    <div class="pull-right" style="padding-right: 15px">
                        <a class="btn btn-danger" style="max-width: 87.48px;" href="@if(session('novoProduto')=='true'){{route('transacao_entradas.transacao_entradas.create')}}{{session()->forget('novoProduto')}}@else{{ route('produtos.produto.index') }}@endif">Cancelar</a>
                        <input class="btn btn-primary" type="submit" value="Cadastrar">
                    </div>
                </div>

            </form>
            @yield('form')
            
        </div>
    </div>

@endsection


